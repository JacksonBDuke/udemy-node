// console.log('Starting app.js');

const fs = require('fs');
const _ = require('lodash');
const yargs = require('yargs');

const notes = require('./notes.js');

const titleOptions = {
    describe: 'Title of note',
    demand: true,
    alias: 't'
}

const bodyOptions = {
    describe: 'The body of the note',
    demand: true,
    alias: 'b'
}

const argv = yargs
    .command('add', 'Add a new note.', {
        title: titleOptions,
        body: bodyOptions
    })
    .command('list', 'List all stored note titles.')
    .command('read', 'Display a note.', {
        title: titleOptions,

    })
    .command('remove', 'Remove a note.',{
        title: titleOptions,
    })
    .help()
    .argv;
var command = process.argv[2];
// console.log('Command: ', command);
// console.log('Process', process.argv);
// console.log('Yargs', argv);

if(command === 'add'){
    var note = notes.addNote(argv.title, argv.body);

    if(note){
        console.log('Added note.');
        notes.logNote(note);
    }
    else{
        console.log('Note title taken.');
    }
}
else if(command === 'list'){
    notes.getAll();
}
else if(command === 'read'){
    var note = notes.readNote(argv.title);

    if(note){
        console.log('Found Note');
        notes.logNote(note);
    }
    else{
        console.log('Note note found.');
    }
}
else if(command === 'remove' || command === 'delete'){
    var removed = notes.removeNote(argv.title);
    var message = removed ? 'Note removed.' : 'No note not found.';
    console.log(message);
}
else{
    console.log('Command not recognized.');
}