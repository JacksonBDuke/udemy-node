// console.log('Starting notes.js');

const fs = require('fs');

var fetchNotes = () => {
    try {
        var notesString = fs.readFileSync('notes-data.json');
        return JSON.parse(notesString);
    } catch (error) {
        return [];
    }
};

var saveNotes = (notes) => {
    fs.writeFileSync('notes-data.json', JSON.stringify(notes));
};

var addNote = (title, body) => {
    var notes = fetchNotes();
    var note = {
        title,
        body
    };
    var duplicateNotes = notes.filter((note) => note.title === title);

    if(duplicateNotes.length === 0){
        notes.push(note);
        saveNotes(notes);
        return note;
    }
    else{
        // console.log(`A note with the title of \"${title}\" already exists. Please use a unique title.`);
    }
};

var getAll = () => {
    console.log('Getting all notes');
    var notes = fetchNotes();
    notes.forEach(noteElement => console.log("-", noteElement.title));
};

var readNote = (title) => {
    var notes = fetchNotes();
    var foundNote = notes.filter((note) => note.title === title);
    return foundNote[0];
};

var removeNote = (title) => {
    var notes = fetchNotes();
    var filteredNotes = notes.filter((note) => note.title !== title);
    // if(notes.length === filteredNotes.length){
    //     console.log(`No notes with the title ${title} found.`)
    // }
    // else{
    //     console.log("Note removed.");
    // }
    saveNotes(filteredNotes);

    return (notes.length !== filteredNotes.length);
};

var logNote = (note) => {
    // debugger;
    console.log('--')
    console.log(`Title: ${note.title}`);
    console.log(`Body: ${note.body}`);
}

module.exports = {
    addNote,
    getAll,
    readNote,
    removeNote,
    logNote
};