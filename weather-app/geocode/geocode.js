const request = require('request');

const key = '&key=AIzaSyALdx0gBplfp4jmEVkKucbQ_XFN7cHLjjY';
const base_url = 'https://maps.googleapis.com/maps/api/geocode/json?address=';

var geocodeAddress = (address, cb) =>{
    var encodedAddress = encodeURIComponent(address);
    var url = base_url + encodedAddress + key;
    request({
        url,
        json: true
    }, (error, response, body) =>{
        // console.log(JSON.stringify(body, undefined, 2));
        if(error){
            cb('Unable to establish connection.');
        }
        else if(body.status === 'ZERO_RESULTS'){
            cb('Unable to resolve address.');
        }
        else if(body.status === 'OK'){
            cb(undefined, {
                address: body.results[0].formatted_address,
                latitude: body.results[0].geometry.location.lat,
                longitude: body.results[0].geometry.location.lng
            });

            // console.log(`Address: ${body.results[0].formatted_address}`);
            // console.log(`Latitude: ${body.results[0].geometry.location.lat}`);
            // console.log(`Longitude: ${body.results[0].geometry.location.lng}`);
        }
    });
};

module.exports = {
    geocodeAddress
}