const yargs = require('yargs');

const geocode = require('./geocode/geocode');
const weather = require('./weather/weather');

const argv = yargs
    .options({
        a: {
            demand: true,
            alias: 'address',
            describe: 'Address to fetch weather for.',
            string: true
        }
    })
    .help()
    .alias('help', 'h')
    .argv;

geocode.geocodeAddress(argv.address, (errorMessage, geo_results) =>{
    if(errorMessage){
        console.log(errorMessage);
    }
    else{
        console.log(geo_results.address);
        weather.getWeather(geo_results.latitude, geo_results.longitude, (errorMessage, current_weather) =>{
            if(errorMessage){
                console.log(errorMessage);
            }
            else{
                console.log(`\tCurrent Temperature: ${current_weather.temperature}${String.fromCharCode(176)}F`); 
                console.log(`\tFeels like: ${current_weather.apparentTemperature}${String.fromCharCode(176)}F`)
                console.log(`\tHumidty: ${current_weather.humidity * 100}%`);
            }
        });
    }
});