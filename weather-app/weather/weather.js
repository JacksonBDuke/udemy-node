const request = require('request');
const base_url = 'https://api.darksky.net/forecast/91032dd9dead5a1558f9bb0611851aaa/';



var getWeather = (lat, lng, cb) =>{
    // var url = base_url + address.latitude + "," + address.longitude;
    request({
        // url,
        url: `${base_url}${lat},${lng}`,
        json: true
    }, (error, response, body) =>{
        // console.log(JSON.stringify(body, undefined, 2));
        if(!error && response.statusCode === 200){
            cb(undefined, {
                temperature: body.currently.temperature,
                apparentTemperature: body.currently.apparentTemperature,
                humidity: body.currently.humidity
            });
        }
        else{
            cb('Unable to fetch weather information from the forecast.io servers.');
        }
    });
};

module.exports = {
    getWeather
}