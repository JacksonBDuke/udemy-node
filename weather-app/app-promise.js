const yargs = require('yargs');
const axios = require('axios')

const g_key = '&key=AIzaSyALdx0gBplfp4jmEVkKucbQ_XFN7cHLjjY';
const google_url = 'https://maps.googleapis.com/maps/api/geocode/json?address=';
const darksky_url = 'https://api.darksky.net/forecast/91032dd9dead5a1558f9bb0611851aaa/'

const argv = yargs
    .options({
        a: {
            demand: true,
            alias: 'address',
            describe: 'Address to fetch weather for.',
            string: true
        }
    })
    .help()
    .alias('help', 'h')
    .argv;

var encodedAddress = encodeURIComponent(argv.address);
// var geocodeUrl = google_url + encodedAddress + g_key;
var geocodeUrl = `${google_url}${encodedAddress}${g_key}`;

axios.get(geocodeUrl).then((response) =>{
    if(response.data.status === 'ZERO_RESULTS'){
        throw new Error('Unable to find that address.');
    }else{
        var lat = response.data.results[0].geometry.location.lat;
        var lng = response.data.results[0].geometry.location.lng;
        var weatherUrl = `${darksky_url}${lat},${lng}`;
        var address = JSON.stringify(response.data.results[0].formatted_address).replace(/['"]/g, '');
        console.log(address);
        return axios.get(weatherUrl);
    }
}).then((response) => {

    var temperature = response.data.currently.temperature;
    var apparentTemperature = response.data.currently.apparentTemperature;
    var humidity = response.data.currently.humidity * 100;
    const degrees = String.fromCharCode(176);

    console.log(`\tCurrent Temperature: ${Math.trunc(temperature)}${degrees}F / ${Math.trunc((temperature - 32) / 1.8)}${degrees}C`); 
    console.log(`\tFeels like: ${Math.trunc(apparentTemperature)}${degrees}F / ${Math.trunc((apparentTemperature - 32) / 1.8)}${degrees}C`);
    console.log(`\tHumidty: ${Math.trunc(humidity)}%`);
}).catch((e) =>{
    // console.log(e);
    if(e.code === 'ENOTFOUND'){
        console.log('ERROR: Could not connect to API servers.')
    }else{
        console.log(e.message);
    }
});