const request = require('request');

const key = '&key=AIzaSyALdx0gBplfp4jmEVkKucbQ_XFN7cHLjjY';
const base_url = 'https://maps.googleapis.com/maps/api/geocode/json?address=';

var geocodeAddress = (address) => {

    var encodedAddress = encodeURIComponent(address);
    var url = base_url + encodedAddress + key;

    return new Promise((resolve, reject) => {
        request({
            url,
            json: true
        }, (error, response, body) =>{
            if(error){
                reject('Unable to establish connection.');
            }
            else if(body.status === 'ZERO_RESULTS'){
                reject("Unable to resolve address.");
            }
            else if(body.status === 'OK'){
                resolve({
                    address: body.results[0].formatted_address,
                    latitude: body.results[0].geometry.location.lat,
                    longitude: body.results[0].geometry.location.lng
                });
            }
        });
    });
};

geocodeAddress('19146').then((location) => {
    console.log(JSON.stringify(location, undefined, 2));
}).catch((errorMessage) => {
    console.log(errorMessage);
});