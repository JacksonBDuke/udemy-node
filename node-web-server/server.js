const express = require('express');
const hbs = require('hbs');

const server_port = 3000;

let app = express();

hbs.registerPartials(__dirname + '/views/partials');
app.set('view engine', 'hbs');
app.use(express.static(__dirname + '/public'));

hbs.registerHelper('getCurrentYear', () => {
    return new Date().getFullYear();
});

hbs.registerHelper('screamIt', (text) => {
    return text.toUpperCase();
})

app.get('/', (request, response) =>{
    // response.send('<h1>Hello World</h1>');
    // response.send({
    //     name: 'Jackson',
    //     occupation: 'Student',
    //     employed: false,
    //     seeking_employment: true
    // })
    response.render('home.hbs', {
        pageTitle: 'Home',
        welcomeMessage: 'Hello and welcome to this website!'
    });
});

app.get('/about', (request, response) =>{
    // response.send('<h1>About Page</h1>');
    response.render('about.hbs',{
        pageTitle: 'About Page',
    });
});

app.get('/bad', (request, response) =>{
    response.send({
        errorMessage: 'Bad request'
    });
})

app.listen(server_port, () =>{
    console.log(`Server is listening on port ${server_port}`)
});